@extends('index')
@section('meta')
    <title>Спасибо</title>
    <meta name="description" lang="ru" content="ДСВ – Інновації для Вашого успіху">

    <meta property="og:title" content="Спасибо">
    <meta property="og:type" content="website">
    <meta property="og:description" content="ДСВ – Інновації для Вашого успіху">
@endsection

@section('content')
    <div class="main">
        <div class="thanks">
            <div class="wrapper">
                <div>
                    <h1>Спасибо</h1>
                    <h4>Ваш заказ отправлен</h4>
                    <p>В ближайшее время с вами свяжется наш менеджер</p>
                </div>
            </div>
        </div>
    </div>
@endsection
