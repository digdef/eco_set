<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->text('address')->nullable();
            $table->string('schedule', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('email_to_send', 255)->nullable();
            $table->string('phone', 255)->nullable();
            $table->string('facebook', 255)->nullable();
            $table->string('vk', 255)->nullable();
            $table->string('instagram', 255)->nullable();
            $table->string('whatsapp', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
